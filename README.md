# dotnetcore-rabbitmq-demo

Demo app showcasing how to send and receive messages using RabbitMQ and .NET Core 3.0, utilizing BackgroundService for a long running task.

To see the documentation for the producer, view the project's [README.md](https://gitlab.com/carrie.dev/dotnetcore-rabbitmq-demo/blob/master/EmailProducerMock/README.md) file.

To see the documentation for the consumer, view the project's [README.md](https://gitlab.com/carrie.dev/dotnetcore-rabbitmq-demo/blob/master/EmailConsumer/README.md) file.

### TODOS:

- [ ] Write log messages to `stdout`.
- [ ] Pass configuration in from environment.
- [ ] Add unit tests.
- [ ] Implement // TODO: notes.
- [ ] Add CI / CD.
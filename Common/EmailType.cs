﻿using System.ComponentModel;

namespace Common
{
    /// <summary>
    /// Email Type to route.
    /// </summary>
    public enum EmailType
    {
        Undefined = 0,
        [Description("Email contains a single recipient.")]
        Single = 1,
        [Description("Email contains a comma delimited list of recipients.")]
        Batch = 2
    }
}

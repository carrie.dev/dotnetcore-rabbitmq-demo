﻿using System;

namespace Common
{
    /// <summary>
    /// IEmailMessage is intended to be the object serialized and passed through a queue.
    /// </summary>
    public interface IEmailMessage
    {
        public int ID { get; set; }
        public Guid CorrelationID { get; set; }
        public string ClientID { get; set; }
        public IEmail Email { get; set; }
        public EmailType Type { get; set; }
    }
}

﻿namespace Common
{
    public interface IEmail
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        // TODO: Add additional properties
    }
}

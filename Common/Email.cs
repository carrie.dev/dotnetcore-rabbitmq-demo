﻿namespace Common
{
    public class Email : IEmail
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }

    public static class EmailExtensions
    {
        public static bool IsValid(this IEmail email)
        {
            bool isValid = true;
            if (string.IsNullOrWhiteSpace(email.From))
            {
                isValid = false;
            }
            else
            {
                // TOOD: Implement business logic here for valid "From".
            }

            if (string.IsNullOrWhiteSpace(email.From) 
                | string.IsNullOrWhiteSpace(email.Subject)
                | string.IsNullOrWhiteSpace(email.Body))
            {
                isValid = false;
            }

            return isValid;
        }
    }
}

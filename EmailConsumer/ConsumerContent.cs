﻿using Common;
using System;

namespace EmailConsumer
{
    /// <summary>
    /// ConsumerContent implements the IEmailMessage structure required to 
    /// digest queue message with additional properties to fill out as needed.
    /// </summary>
    public class ConsumerContent : IEmailMessage
    {
        public int ID { get; set; }
        public Guid CorrelationID { get; set; }
        public string ClientID { get; set; }
        public IEmail Email { get; set; }
        public EmailType Type { get; set; }

        // Additional properties:
        public int SenderReputationScore { get; set; }
        public int MessageReputationScore { get; set; }
    }
}

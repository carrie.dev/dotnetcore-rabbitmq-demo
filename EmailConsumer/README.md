﻿# Email Router Service

## Overview

The Email Router service receives messages off of a queue via AMQP:

```mermaid
graph LR;
    A[Single Emails API] -->|AMQP| C(Emails Exchange)
    B[Batch Emails API] -->|AMQP| C(Emails Exchange)
    C --> D(Emails-Router Queue)
    D --> | AMQP Push | E((Email-Router Service))
```

The service then processes the content for validity and routes appropriately:

```mermaid
graph LR;
E((Email-Router Service)) --> F{Valid 'From' Address}
F --> |Yes| G{Email Content Approval}
F --> |No| H(Emails-Error Exchange)

G --> |Yes| J{Review Email Type}
G --> |No| H

J --> |Single| K(Send-Single Exchange)
J --> |Bulk| L(Send-Bulk Exchange)
J --> |Unknown| H
```

The content of the Email Router could be broken out into more single responsibilities, but with that comes more overhead to maintain and support.

## Local Setup
To spin up a local instance of RabbitMQ, use the following docker command:
`docker run -p 5672:5672 -p 15672:15672 rabbitmq:management`

Then open the management page - http://localhost:15672 and login with the default of name: `guest` & password: `guest`.

Run the EmailProducerMock project to create a message on the "Emails-Route" queue.

Then run this project to process the message off of the "Emails-Route" queue.

## Scalability

To scale the Email Router service, there are a vast amount of possibilities. For example, the service could simply scale horizontally. More complex would be to pull out the validation and content review steps into separate services that are accessed via gRPC as an example that could be separately scaled if that was the most time-consuming process. Additional exchange/queues could be implemented to further scale and create more single responsible units.


## References
- To view mermaid diagrams in preview mode from vscode, [install extension](https://marketplace.visualstudio.com/items?itemName=bierner.markdown-mermaid).
- To view markdown files easier including mermaid preview from visual studio, [install extension](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.MarkdownEditor).
- Used [this article](https://www.c-sharpcorner.com/article/consuming-rabbitmq-messages-in-asp-net-core/) to spin up the consumer for RabbitMQ quickly using .NET Core BackgroundService.
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace EmailConsumer
{
    /// <summary>
    /// The Worker class is responsible for consuming messages off of a queue via AMQP.
    /// </summary>
    public class Receiver : BackgroundService
    {
        private const string QueueName = "email-router";

        private readonly ILogger<Receiver> _logger;
        private IConnection _connection;
        private IModel _channel;

        public Receiver(ILogger<Receiver> logger)
        {
            _logger = logger;
            InitRabbitMQ();
        }

        /// <summary>
        /// Initialize RabbitMQ - to learn more, go to rabbitmq.com/dotnet-api-guide.html.
        /// </summary>
        private void InitRabbitMQ()
        {
            var factory = new ConnectionFactory { HostName = "localhost" };

            // Create connection to RabbitMQ.
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

            // Create and hook up the exchange and queue. Note this is just here to show how, but not needed in
            //  this demo and would tightly couple the exchange to the queue implementation which is not recommended.
            //_channel.ExchangeDeclare("emails", ExchangeType.Direct);
            _channel.QueueDeclare(QueueName, false, false, false, null); // Not commented out to not crash if this was ran prior to "EmailProducerMock" which creates the queue.
            //_channel.QueueBind("emails-router", "emails", "emails-router", null);

            // Tell consumer to only prefetch one message at a time so other service instances can take the load.
            // To learn more, go to rabbitmq.com/consumer-prefetch.html.
            _channel.BasicQos(0, 1, false);

            _connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            // Receive messages by subscription aka push API. 
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (ch, ea) =>
            {
                IEmailMessage message = null;
                try
                {
                    // received message
                    message = JsonConvert.DeserializeObject<IEmailMessage>(Encoding.UTF8.GetString(ea.Body));
                }
                catch
                {
                    // DO NOT log content as it could be malicious and we do not want to store for further destruction.
                    _logger.LogWarning($"Invalid message structure received from queue: {QueueName}.");
                    // Swallow exception and ack msg off queue as it will never process correctly.
                }

                try
                {
                    HandleMessage(message);

                    // Acknowledge message off of queue.
                    _channel.BasicAck(ea.DeliveryTag, false);
                }catch
                {
                    // Something unexpected happened.

                    // TODO: Sleep process for configurable amount of time to not instantly reprocess same  
                    // message again as it most likely was infrastructure/network related.

                    // Leave message on queue to have processing attempted at a later time.
                    _channel.BasicNack(ea.DeliveryTag, false, true);
                }
            };

            consumer.Shutdown += OnConsumerShutdown;
            consumer.Registered += OnConsumerRegistered;
            consumer.Unregistered += OnConsumerUnregistered;
            consumer.ConsumerCancelled += OnConsumerConsumerCancelled;

            _channel.BasicConsume(QueueName, false, consumer);
            return Task.CompletedTask;
        }

        private void HandleMessage(IEmailMessage message)
        {
            // Validate content is correct format.
            if (!message.Email.IsValid()) {
                _logger.LogInformation($"Invalid email content in message ID: {message.ID} from address: {message.Email.From}.");

                // TODO: Create message on "Emails-Error" exchange to notify additional recipients.  Message will be ack off "Emails-Route" queue as nothing further to do.
                return;
            }
                        
            if (!IsSubscribed(message.ClientID, message.Email.To))
            {
                _logger.LogInformation($"Remove to: {message.Email.To} from sender: {message.Email.From}.");

                // TODO: Create message on "Emails-Unsubscribe" exchange to notify sender.  Message will be ack off "Emails-Route" queue as nothing further to do.
                return;
            }

            if (!IsContectAcceptable(message.Email.Subject, message.Email.Body))
            {
                _logger.LogInformation($"Notify client: {message.ClientID} of unappropriate email content in message ID: {message.ID}.");

                // TODO: Create message on "Emails-Error" exchange to notify sender.  Message will be ack off "Emails-Route" queue as nothing further to do.
                return;
            }

            // TODO: Deal with reputation score at sender level & message level.
            var score = CheckReputationScore(message);

            // TODO: Convert logic below to individual entities that have encapsulated logic instead of so much logic branching.

            switch (message.Type)
            {
                case EmailType.Single:
                    if (score > 100)
                    {
                        // TODO: Create message on "Emails-Single-Priority" exchange to send email.
                    } else
                    {
                        // TODO: Create message on "Emails-Single" exchange to send email.
                    }
                    break;
                case EmailType.Batch:
                    if (score > 100)
                    {
                        // TODO: Create message on "Emails-Batch-Priority" exchange to send email.
                    }
                    else
                    {
                        // TODO: Create message on "Emails-Batch" exchange to send email.
                    }
                    break;
                case EmailType.Undefined:
                    _logger.LogWarning($"Invalid sender type, unable to process message ID: {message.ID} with correlationID: {message.CorrelationID}.");
                    // TODO: Create message on "Emails-Error" exchange to notify additional recipients.  Message will be ack off "Emails-Route" queue as nothing further to do.
                    break;
            }

            // Just for demo purposes ONLY!
            _logger.LogInformation($"consumer processed {message}");
        }

        #region BusinessLogic to refactor out of this class :)

        private bool IsSubscribed(string clientID, string userEmail)
        {
            if (string.IsNullOrWhiteSpace(clientID) | string.IsNullOrWhiteSpace(userEmail))
            {
                return false;
            }

            // TODO: Offload work to another queue or gRPC accessible microservice for more performance and scalability.
            return true;
        }

        private bool IsContectAcceptable(string subject, string body)
        {
            if (string.IsNullOrWhiteSpace(subject) | string.IsNullOrWhiteSpace(body))
            {
                return false;
            }

            // TODO: Offload work to perform email content validation via "Natural Language Processing"
            // to another queue or gRPC accessible microservice for more performance and scalability.
            return true;
        }

        private int CheckReputationScore(IEmailMessage message)
        {
            // TODO: Offload work to another queue or gRPC accessible microservice for more performance and scalability.

            int score = 150;
            return score;
        }

        #endregion

        #region RabbitMQ Events to refactor out of this class :)
        private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e)
        {
            _logger.LogInformation($"connection shut down {e.ReplyText}");
        }

        private void OnConsumerConsumerCancelled(object sender, ConsumerEventArgs e)
        {
            _logger.LogInformation($"consumer canceled {e.ConsumerTag}");
        }

        private void OnConsumerUnregistered(object sender, ConsumerEventArgs e)
        {
            _logger.LogInformation($"consumer unregistered {e.ConsumerTag}");
        }

        private void OnConsumerRegistered(object sender, ConsumerEventArgs e)
        {
            _logger.LogInformation($"consumer registered {e.ConsumerTag}");
        }

        private void OnConsumerShutdown(object sender, ShutdownEventArgs e)
        {
            _logger.LogInformation($"consumer shutdown {e.ReplyText}");
        }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }
        #endregion
    }
}
﻿using Common;
using System;

namespace EmailProducerMock
{
    /// <summary>
    /// Message implements the IEmailMessage structure required to 
    /// enqueue messages so consumers know what to expect.
    /// </summary>
    public class Message : IEmailMessage
    {
        public int ID { get; set; }
        public Guid CorrelationID { get; set; }
        public string ClientID { get; set; }
        public IEmail Email { get; set; }
        public EmailType Type { get; set; }
    }
}

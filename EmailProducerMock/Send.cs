﻿using Common;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Text;

namespace EmailProducerMock
{
    /// <summary>
    /// Send console app mocks what a real application would do to put messages onto RabbitMQ's exchange that would be routed to approriate queue.
    /// </summary>
    class Send
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "emails-router",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
                
                IEmailMessage emailMsg = new Message
                {
                    ID = 123,
                    ClientID = "domain.com",
                    CorrelationID = new Guid(),
                    Type = EmailType.Single,
                    Email = new Email
                    {
                        From = "me@domain.com",
                        To = "you@domain.com",
                        Subject = "Hello Old Friend!",
                        Body = "This is the song that does not end...."
                    }
                };

                string msg = JsonConvert.SerializeObject(emailMsg);

                var body = Encoding.UTF8.GetBytes(msg);

                channel.BasicPublish(exchange: "",
                                     routingKey: "emails-router",
                                     basicProperties: null,
                                     body: body);
                Console.WriteLine(" [x] Sent {0}", msg);
            }

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
    }
}

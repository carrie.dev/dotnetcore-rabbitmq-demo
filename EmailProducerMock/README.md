﻿# Email Producer Mock

This project was created as a test console only to send objects to RabbitMQ's exchange.

Code derived from [RabbitMQ's C# tutorials](https://www.rabbitmq.com/getstarted.html).


## Local Setup
To spin up a local instance of RabbitMQ, use the following docker command:
`docker run -p 5672:5672 -p 15672:15672 rabbitmq:management`

Then open the management page - http://localhost:15672 and login with the default of name: `guest` & password: `guest`.